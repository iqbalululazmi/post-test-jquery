// <======== Jquery + jquery validator =========>
$(document).ready(function () {
  $('#formLogin').validate({
    rules: {
      username: {
        required: true,
        minlength: 8,
      },
      password: {
        required: true,
        minlength: 8,
      },
    },
  });

  $('#formLogin').on('submit', function (e) {
    e.preventDefault();

    const formIsValid = $('#formLogin').valid();
    if (formIsValid) {
      alert('Successfully login');
      window.location.href = '/';
    }
  });
});

// <====== manual =======>
// $(document).ready(function () {
//   $('#formLogin').on('submit', function (e) {
//     const inputUsernameValue = $('#username').val();
//     const inputPasswordValue = $('#password').val();

//     if (isFormValid(inputUsernameValue, inputPasswordValue)) {
//       e.preventDefault();
//       alert('Successfully login');
//       window.location.href = '/';
//     }
//   });

//   const isFormValid = (username, password) => {
//     if (username === '') {
//       alert('username harus diisi');
//       return false;
//     } else if (username.length < 8) {
//       alert('Username minimal 8 karakter');
//       return false;
//     } else if (password === '') {
//       alert('Password harus diisi');
//       return false;
//     } else if (password.length < 8) {
//       alert('Password minimal 8 karakter');
//       return false;
//     }
//     return true;
//   };
// });
